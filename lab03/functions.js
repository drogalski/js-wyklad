// hoisting (wynoszenie na samą górę deklaracji funkcji)
// deklaracja funkcji zadziała gdy funkcja jest pod jej wywołaniem
// przypisanie funkcji do zminnej pod jej wykonaniem zwróci błąd inicjalizacji

// Scope - variable resolving (skąd pobierać wartość zmiennej)

// JS: funkcyjny i leksykalny, let - zasięg blokowy

// nie wolno deklarować zmiennych globalnych np. w = 5;

const multiply = function (a, b) {
    const w = 5;

    if (w > 3) {
        let message = "W > 3";
        console.log(message);
    }    return a * b + w;
};

console.log(multiply(2, 3));

function myFunction() { // nie trzeba deklarować argumentów w nawiasie ()

    console.log(arguments); // wyświetla argumenty funkcji

    return arguments[0];
}

myFunction(2, 3, 6, "Hello");