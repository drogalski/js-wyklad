'use strict';
// reducer - funkcja, która coś robi ze zgromadzoną zawartością (przechodzi po każdym elemencie)

const sumReducer = (acc, elem) => acc + elem;
const multiplyReducer = (acc, elem) => acc * elem;

// (funkcja, stan początkowy akumulatora, tablica)
const reduce = (reducer, accInit, arr) => {
  let acc = accInit;
  // tak też można zapisać tę pętlę, ale z 'use strict' nie działa, bo "length is not defined"
  // for (let i = 0; {length} = arr, i < length; i++ ) {
  //   acc = reducer(acc, arr[i]);
  // }
  for (let i = 0; i < arr.length; i++ ) {
    acc = reducer(acc, arr[i]);
  }
  return acc;
};

console.log(reduce(sumReducer, 0, [1, 2, 3, 4]));
console.log(reduce(multiplyReducer, 1, [1, 2, 3, 4]));

const imiona = ["Bolek", "Lolek", "Tola"];
const helloReducer = (acc, elem) => [...acc, "Hello " + elem];

const result = reduce(helloReducer, [], imiona);
console.log(result);

const myObj = {
  imie: 'chat',
  nazwisko: 'GPT',
  rezultat: 'Strzał w nadgarstek programisty'
};

const {imie} = myObj;
// to samo inaczej:
// const imie = myObj.imie;
const {rezultat} = myObj;
console.log(imie, myObj.nazwisko, " = ", rezultat);

const filter = (predicat, arr) => reduce(
  (acc, elem) => predicat(elem) ? [...acc, elem] : acc,
  [],
  arr
);
