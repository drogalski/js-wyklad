// Ogólny schemat

function f(a) {
    // ...
    return function g(b) {
        /// ...
        return a + b
    };
}

console.log(f(2)(5));


const incBy3 = f(3);
const incBy7 = f(7);


console.log(incBy3(7));
console.log(incBy7(7));

const inc = f(1);

console.log(inc(6));