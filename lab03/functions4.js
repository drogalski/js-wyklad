// Lambda, fat arrow



const multiply = (a, b) => a * b;
const multiplyOld = function (a, b) {
    return a + b;
};
multiply(3, 4);

const inc = a => a + 1;
const incOld = function (a) {
    return a + 1;
}
inc(3);

//------------------
function sumOld(a, b) {
    return a + b;
}

//------- Curried function
const sum = a => b => a + b;
sum(2)(3);

function sum(a) {
    return function(b) {
        return a + b
    }
}