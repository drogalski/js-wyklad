// Closure

function f(a) {
    let s = 1;

    return function(b) {
        s++;
        return a + b + s;
    }
}

const incByX = f(2);

const result1 = incByX(3);
console.log(result1);

const result2 = incByX(3);
console.log(result2);

const result3 = incByX(3);
console.log(result3);

// Immediately Executed Function 

const inc = (function () {
    let i = 0;
    return function() {
        const prev = i;
        i = i + 1;
        return prev;
    };
})(777);

console.log(inc());
console.log(inc());
console.log(inc());   
