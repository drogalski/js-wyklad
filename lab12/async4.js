const axios = require('axios');

let myResult = '';

axios
    .get('https://jsonplaceholder.typicode.com/posts/2')
    .then(result => {
        myResult = result.data;
        console.log(myResult);
    })

console.log(myResult);