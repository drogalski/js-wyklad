const axios = require('axios');

const myPromise1 = new Promise( (resolve, reject) => {
    // ... logika związana z przetwarzaniem
    // if () reject("Error");
    axios
        .get('https://jsonplaceholder.typicode.com/posts/2')
        .then(result => {
            resolve(result.data)
    })
});

myPromise1
    .then(value => value.title)
    .then(value => console.log(value));


const myPromise2 = Promise.resolve(34);

myPromise2
    .then(value => {return value * 2})
    .then(value => value + 1) // nie musimy tu uzywać return, bo jeśli jest tylko value to jest domyślnie zwracane
    .then(value => console.log(value));
