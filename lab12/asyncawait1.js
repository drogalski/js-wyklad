const axios = require('axios');


(async function() {

    const myPromise1 = new Promise(
        (resolve, reject) => setTimeout(resolve, 3000, "foo"));

    const myPromise2 = new Promise(
        (resolve, reject) => setTimeout(resolve, 5000, "bar"));
        
    const async1 = async () => {
        return "foo";
    };

    const postID = await axios
    .get('https://jsonplaceholder.typicode.com/posts/6')
    .then(response => response.data.id);

    const v1 = await myPromise1;
    console.log(v1);
    const v2 = await myPromise2;
    console.log(v2);

    const value = await async1();

    console.log(value);
    console.log(postID);


// async1().then(value => console.log(value));

})()