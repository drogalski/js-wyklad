// hoisting (wynoszenie na samą górę deklaracji funkcji)
// deklaracja funkcji zadziała gdy funkcja jest pod jej wywołaniem
// przypisanie funkcji do zminnej pod jej wykonaniem zwróci błąd inicjalizacji

// Scope - variable resolving (skąd pobierać wartość zmiennej)

// JS: funkcyjny i leksykalny, let - zasięg blokowy

// nie wolno deklarować zmiennych globalnych np. w = 5;

let multiply = function (a, b) {
    const w = 5;
    return a * b + w;
}

console.log(sum(2, 3));
console.log(multiply(2, 3));
console.log(w);

function sum(a, b) {
    return a + b;
}

// obiekt z globalnymi "śmieciami" i globalnymi zmiennymi
console.log(global.w);