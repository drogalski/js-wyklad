const n = ["pies", 1, true];

n[99] = "Hello";

// Destrukturyzacja (Destructuring assingment)

const { length } = n;

console.log(length);

const o = {
    firstName: 'Jan',
    lastName: 'Przyslowiowy-Kowalski',
    isStudent: false
};

console.log(o.isStudent); // opcjonalnie console.log(o['isStudent']);

delete o.isStudent;

console.log(o);

o.yob = 1964;

console.log(o);

const osoba4 = {
    imie: 'Jan',
    rokUr: 1967
};

const osoba5 = {
    imie: 'Janina',
    studiuje: true
};

// Destrukturyzacja (Spread)

const osoba6 = {...osoba4, ...osoba5, wzrost: 167, imie: 'Ewa'};
const copy = {...osoba4};

let ala;
let ma;
let kota;
const osoba7 = Object.assign({imie: "Zenon"}, ala, ma, kota, {rokUr: 1945}); // {} na początku jest potrzebne dla null i undefined
console.log(osoba7);
console.log(osoba6);

// Destrukturyzacja na tablicy

const [one, two] = [1, 2, 3];
console.log(one);
console.log(two);

const {imie, rokUr} = osoba7;

console.log(imie);
console.log(rokUr);

const marka = 'Fiat';
const model = 'Uno';

// const auto = {marka: marka}; (dla klucza nazwanego tak samo jak zmienna z wartością wystarczy podać samą zmienną bez klucza
const auto = {marka, model};

console.log(auto);