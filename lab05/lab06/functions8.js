const fp = require('lodash/fp')

// iterates-first data-last methods (styl funkcyjny, polecany :) )

const res = fp.flow(
    fp.map(x => x * 2),
    fp.map(x => x + 1)
    )
    ([1, 2, 3, 4]);

const res2 = [1, 2, 3, 4].map(x => x * 2).map(x => x + 1)

console.log(res);
console.log(res2);

// Styl imperatywny - bardziej mówimy "JAK" ma działać
const doubler = numbers => {
    let result = [];
    for (let i = 0; i < numbers.length; i++) {
        result.push(numbers[i] * 2);
    }
    return result;
};

console.log(doubler([1, 2, 3]));

// Styl funkcyjny - bardziej mówimy "CO" ma się dziać
const doublerF = numbers => numbers.map(x => x * 2);

console.log(doublerF([1, 2, 3]));