const osoba = {
    name: "John",
    yob: 1971,
    sayHello: function() {
        return `Hello ${this.name} (${this.yob})`;
    }
};

console.log(osoba.sayHello());

const sayH = function(day, year) {
    return `Hello ${this.name} (${this.yob}). Today is ${day}. Year ${year}`;
};

console.log(sayH());

const res = sayH.apply({name: "Mary", yob: 1967}, ["Friday", 1945]);
const res2 = sayH.call({name: "Mary", yob: 1956}, "Friday", 1945);

console.log(res);
console.log(res2);


function Person(name, yob) {
    this.name = name;
    this.yob = yob;
}

const p = new Person("Jan", 1956);

console.log(p);