'use strict';

const fp = require('lodash/fp');
const _ = require('lodash');

const inc = x => x + 1;
const double = x => x * 2;

// Algebra (f * g)(x) = f(g(x))

const res = inc(double(inc(5)));

// const compose = (fn1, fn2, fn3, x) => fn1(fn2(fn3(x)))
const compose = (...fns) => x => fns.reduceRight((acc, fn) => fn(acc), x)

const trace = message => value => {
    console.log(`------- ${message}: ${value} -------`);
    return value;
}

const res2 = compose(inc, trace('Pod double i inc'), double, trace('Pod pierwszym inc'), inc, x => x * 3, double)(5);

console.log(res2);

const pipe = (...fns) => x => fns.reduce((acc, fn) => fn(acc), x)

const res3 = pipe(inc, trace('Po inc'), double, trace('Po inc i double'), inc, x => x * 3, double)(5);

console.log(res3);

const sum = (a, b) => a + b;
const curriedSum = a => b => a + b;

sum(3, 2);
curriedSum(3)(2);

const mySum = (a, b, c) => a + b + c;

const mySumCurried = _.curry(mySum);

console.log(mySumCurried(2)(3)(4));
console.log(mySumCurried(2, 3)(4));

const res5 = fp.compose(inc, trace('Pod double i inc'), double, trace('Pod pierwszym inc'), inc, x => x * 3, double)(5);

console.log(res5);

const res6 = fp.pipe(inc, trace('Po inc'), double, trace('Po inc i double'), inc, x => x * 3, double)(5);

console.log(res6);
