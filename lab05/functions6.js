'use strict';

const _ = require('lodash');

const myList = [
  { id: "uuid1", imie: "Jan", nazwisko: "Kowalski" },
  { id: "uuid2", imie: "Janina", nazwisko: "Cowalsky" },
  { id: "uuid3", imie: "Grażynka", nazwisko: "Cowalski" },
  { id: "uuid4", imie: "Bożenka", nazwisko: "Kowalsky" },
];

const myObj = {
  "uuid1": {
    imie: "Jan", nazwisko: "Kowalski"
  },
  "uuid2": {
    imie: "Janina", nazwisko: "Cowalsky"
  }
};

// funckja, która przekształca myList na myObj - wykład 5 24.03.23
const myObjects = myList.reduce((acc, elem) => ({
    ...acc,
    [elem.id]:  _.omit(elem, ['id'])

}), {});

console.log(myObjects);