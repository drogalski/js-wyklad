// instrument denty od dentysty np. Puzon

// Prototype. Protortpal inheritance

const obj = {
    name: 'John'
};

function Person(name) {
    this.name = name;
}

const panJan = new Person("Jan");

Person.prototype.sayHello = function () {
    return `Hello I'm ${this.name}`
}

Object.prototype.toString = function () {
    return `[ name = ${this.name}]`
}

console.log(panJan.toString());


const Animal = function(name) {
    this.name = name;
};

Animal.prototype.printName = function () {
    console.log(this.name);
}

const abstractAnimal = new Animal("nobody");
abstractAnimal.printName();

const Mammal = function (name, yob) {
    Animal.call(this, name);
    this.yob = 2020;
}

// nie jest to eleganckie rozwiązanie (nie stosować)
Mammal.prototype = Object.create(Animal.prototype);
Mammal.prototype.constructor = Mammal;
Mammal.prototype.getAge = function () {
    return new Date().getFullYear() - this.yob;
}

const Dog = function (name, breed, yob) {
    Mammal.call(this, name, yob);
    this.breed = breed;
}

Dog.prototype = Object.create(Mammal.prototype);
Dog.prototype.constructor = Mammal;

const myDog = new Dog("Reksio", "cundelburry", 1985);
myDog.printName()
console.log(myDog.getAge());

const abstractMamal = new Mammal("some mamal", 2020);
abstractMamal.printName()

class Shape {
    constructor(name = 'Abstract shape') {
        this.name = name;
    }

    get description() {
        return this.name;
    }
}

class Rectangle extends Shape {
    constructor(height, width, name ='Rectangle') {
        super(name);
        this.height = height;
        this.width = width;
    }
    // overriding
    get description() {
        return `${this.name} is ${this.height} x ${this.width}`;
    }
}

class Square extends Rectangle {
    constructor(length) {
        super(length,length,"Square");
    }
}

const myRectangle = new Rectangle(3, 2);
console.log(myRectangle.description);

const mySquare = new Square(6);
console.log(mySquare.description);