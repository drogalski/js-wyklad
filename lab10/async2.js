const myPromise = new Promise((resolve, reject) => {
    setTimeout(() => resolve("jeden"), 2000)
});

const createPromise = (time, result) => new Promise((resolve, reject) => {
        console.log("Przed setTimeout: " + result);
        setTimeout(() => {
            console.log("Upłynął czas, wypełniam promise");
            resolve(result)}, time);
        console.log("Po setTimeout: " + result);
    }
);

createPromise(1000, "jeden")
    .then(result => createPromise(2000, result + result))
    .then(result => createPromise(1000, result + result + result));

// myPromise.then(result => {
//     console.log(result)
//     return new Promise((resolve, reject) => {
//         setTimeout(() =>
//         resolve("dwa"), 3000)
//     })
// }).then(result => console.log(result));
// console.log("start");


// Potrzebna biblioteka - AXIOS (bardzo ładnie wykonuje się requesty asynchroniczne)
// bedziemy jej uzywac na zajęciach z JS'a jak i na front-end developmencie w przyszłym roku