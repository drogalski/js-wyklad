// poziom zagniezdzenia moze być bardzo głęboki (kod trudny w czytaniu), odchodzi się od takiego rozwiązania
// do kazdej funkcji trzeba przewidzieć obsługę błędów (err)
queryDB(queryParams, function (err, queryResult) {
    // obsługa result
    doHttpRequest(queryResult.url, function (err, response) {
        saveToFile(response.data, function (err, status) {
            console.log(status);
        });
    });
});

// zmiana kodu zagniezdzonego w liniowy (słowo kluczowe then())
queryDB(queryParams)
    .then(function (queryResult) {

    })
    .then(function (response) {
        
    })
    .then(function (data) {
        
    })
    .catch(function (err) {
        
    });


// przechodzimy z rozwiązania callback'owego na rozwiązanie Promise'owe
queryDB(queryParams)
    .then(queryResult => {
        //...
        return new Promise() // Promise to typ wbudowany w język API (klasa), który posiada róznego rodzaju operacje np. then()
    })
    .then(response => {
        //...
        return new Promise()
    })

// w codziennej pracy uzywa się róznych konstrukcji do obsługi ządań http z bibliotek, które są typu Promise