// Model przetwarzania (zdarzeń, ządań) oparty o obliczenia nieblokujące i asynchroniczne


// Źródła:
// -Dyskusje i prezentacje w internecie
// -Dokumentacja techniczna
// -Kurs "Principles of Reactive Programming"
// -Własne doświadczenia, przemyślenia, pytania


// Zadanie:
// Pozyskać zawartość pliku (np. tekstowego) z dysku i zrobić z nią coś (np. przekazać zawartość)

// "Tradycyjny", oczywisty sposób
var contents = fs.readFileSync('/opt/plik') // funkcja z biblioteki node'owej (fs - file system)
// wykonuję czynność z zawartością
console.log(contents);

// "Nowy" sposób
fs.readFile('opt/plik', function(err, contents){ // readFile bierze callback a readFileSync nie
    // ...robię coś z zawartością
    console.log(contents);
});



// Przykładowe pytania

// Obliczenia rozproszone, równoległe:
//  -Ile procesów (procesy są cięzkie, kazdy ma ochronę w systemie)
//  -Ile wątkow (wątki mogą się zakleszczać, czekają na swoje wzajemne działania i mają wspólną pamięć, można je zagłodzić - któryś nigdy nie dojdzie do głosu)
//  -Jak operacje I/O wpływają na czas obliczeń (najmocniej wpływają poprzez np. odczyt danych z pliku)

// Jaki model architektury/aplikacji wybrać aby zapewnić dobre skalowanie (aka myślenie przyszłościowe :)
//  -Twoja aplikacja słabo się skaluje
//  -Serwery (sprzęt, chmura, skalowanie poziome), serwer aplikacji, platforma językowa


// Obsługa ządań

// Tradycyjne, sprawdzone, uznane podejście
//  -Synchroniczne, blokujące IO
//  -Single request per thread
//  -Wiele wątków (rzędy tysięcy)
//  -Dobrze znane, choć niełatwe problemy związane z modelem wielowątkowym
//  -Sprawdza się do ok. 1000 (jednoczesnych) klientów

// Świat się jednak zmienia...

// Szaleństwo skalowalności (micro)benchmarków (przypominam zarzut: Twoja aplikacja słabo się skaluje)
// Z rorzmaitych (a moe oczywistych?) powodów zaczynamy mówić o aplikacjach obsługujących miliony zdarzań/ządań

// Gdzie szukać oszczędności
// (Zdjęcia w telefonie)


// 150ms - tyle mniej więcej zajmuje przesłanie pakietu metodą GET do np. USA (obserwujemy w sklepach Online)
// kazdy wątek ma swój przydział pamięci (przewaznie 4KB)


// POMYSŁ !!!
//  -Serwery typu "single-threaded" (np. node)
//  -"Everything runs in parallel except your code"
//  -Pojawia się wysokopoziomowa (serwerowa) architektura oparta o "event-loop"

// Event loop
// (zdjęcie w telefonie)


// Ćwiczenie Asynchroniczne (kod asynchroniczny)

const foo = () => console.log("First");
const bar = () => setTimeout(() => console.log("Second"), 500);
const baz = () => console.log("Third");

bar();
foo();
baz();

// klasy dostarczające API (np. Promise), które łatwo dostarczają: 
// dostęp do bazy => wysłanie response => zapytanie do bazy => wykonanie czynności