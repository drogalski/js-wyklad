const z = () => console.log("ZZZZ");

const g = () => {
    console.log("Before z");
    z();
    console.log("After z");
};

// g();

const g2 = () => {
    console.log("Before z");
    setTimeout(() => console.log("Executing callback"), 2000);
    console.log("After z");
}

// g2();

const freezer = (seconds) => {
    console.log("Freezer started");
    const waitTo = new Date().getTime() + seconds * 1000;
    while (waitTo > new Date().getTime()) {}
    console.log("Freezer finished");
}

const g3 = () => {
    console.log("Before f");
    freezer(10);
    console.log("After f");
}

setTimeout(() => console.log("Executing callback1"), 2000);
setTimeout(() => freezer(40), 3000);
g3();
setTimeout(() => console.log("Executing callback3"), 4000);
setTimeout(() => console.log("Executing callback4"), 1000);
setTimeout(() => console.log("Executing callback5"), 1000);
setTimeout(() => console.log("Executing callback6"), 3000);