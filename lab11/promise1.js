const axios = require('axios');

const promise = new Promise((resolve, reject) => {
    setTimeout(resolve, 3000, "foo");
});

const promise2 = Promise.resolve(32);

const req1 = axios.get("https://jsonplaceholder.typicode.com/posts");
const req2 = axios.get("https://jsonplaceholder.typicode.com/users");

Promise.all([req1, req2]).then(values => {
    values.map(value => console.log(value.data));
});

Promise.any([req1, req2]).then(value => {
    console.log(value.data);
});

Promise.race([req1, req2]).then(value => {
    console.log(value.data);
});